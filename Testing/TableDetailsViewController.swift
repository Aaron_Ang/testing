//
//  TableDetailsViewController.swift
//  Testing
//
//  Created by Aaron Ang on 07/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class TableDetailsViewController: UIViewController {
    var park: Park!

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.nameLabel.text = self.park.name
        self.coverImageView.image = self.park.photo
        self.ratingLabel.text = "\(self.park.averageRating())"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
