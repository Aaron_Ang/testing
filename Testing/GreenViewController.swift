//
//  GreenViewController.swift
//  Testing
//
//  Created by Aaron Ang on 05/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func showBlueVC(sender: AnyObject) {
        let BlueVC: BlueViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BlueViewController") as! BlueViewController
        
        self.presentViewController(BlueVC,animated: true, completion:nil)
    }

    @IBAction func showRedVc(sender: AnyObject) {
            let RedVC: RedViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RedViewController") as! RedViewController
            
            self.navigationController?.pushViewController(RedVC, animated: true)
        
    }
}
