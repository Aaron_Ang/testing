//
//  TricksOfTableViewController.swift
//  Testing
//
//  Created by Aaron Ang on 07/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class TricksOfTableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    //Table sections
    var fruits: [String] = ["Apple" , "Banana" , "Mango" , "Orange" , "Watermelon" , "Cherry" , "Coconut"]
    var cars: [String] = ["Toyota" , "Ford" , "Honda" , "Nissan" , "Proton" , "Subaru" , "Ferrari" , "BMW"]
    //Section headers
    var letters: [String] = []
    var countries: [String] = ["Albania" , "Bahamas" , "Bahrain" , "Colombia" , "China" , "Dominica" , "East Timor" , "Fiji" , "Gabon" , "Africa" , "Afghanistan" , "Honduras" , "Iceland" , "Japan" , "Kenya" , "Kenya"]
    var countriesByLetter: [String : [String]] = [:]
    //4. Multiple selections
    var selectedCountries: [String] = []
    //3. pull to refresh
    var refeshControl: UIRefreshControl!
    
    func sortCountries() {
        for name in self.countries {
            let firstLetter: String = name.substringToIndex(name.startIndex.advancedBy(1))
            if self.countriesByLetter[firstLetter] == nil {
                letters.append(firstLetter)
                self.countriesByLetter[firstLetter] = [name]
            } else {
                self.countriesByLetter[firstLetter]!.append(name)
            }
        }
    }
    //7. Edit mode
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortCountries()
        //7. Edit mode
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
        //3. Pull to refresh
        self.refeshControl = UIRefreshControl()
        //self.refeshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.refeshControl.addTarget(self, action: #selector(TricksOfTableViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refeshControl)
    }
    //3. Pull to refresh
    func refresh() {
        sleep(2)
        self.refeshControl.endRefreshing()
    }
}

//10. Table sections
//Set the tableView's style to Grouped
/*extension TricksOfTableViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return fruits.count
        } else {
            return cars.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        
        if indexPath.section == 0 {
            let fruit = self.fruits[indexPath.row]
            cell.textLabel?.text = fruit
        } else {
            let car = self.cars[indexPath.row]
            cell.textLabel?.text = car
        }
        return cell
    }
    
}*/

//Set the tableView's style to plain
extension TricksOfTableViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.letters.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let firstLetter = self.letters[section]
        return self.countriesByLetter[firstLetter]!.count
    }
 
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        let firstLetter = self.letters[indexPath.section]
        let countryName = self.countriesByLetter[firstLetter]![indexPath.row]
        
        cell.textLabel?.text = countryName
        //5. Cell accessory view
        //cell.accessoryType = UITableViewCellAccessoryType.DetailDisclosureButton
        
        //4. Multiple selections
        if selectedCountries.contains(countryName) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
    }
 
//9. Section Headers and Footers
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let firstLetter = self.letters[section]
        return firstLetter
    }
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let firstLetter = self.letters[section]
        return "\(countriesByLetter[firstLetter]!.count) Countries Starting with \(firstLetter.capitalizedString)"
    }
//8. Section indices
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return self.letters
    }
//6. Swipe to delete
    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "Bye!"
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let firstLetter = self.letters[indexPath.section]
            self.countriesByLetter[firstLetter]!.removeAtIndex(indexPath.row)
            self.tableView.reloadData()
        }
    }
//5. Cell accessory view
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let firstLetter = self.letters[indexPath.section]
        let countryName =  self.countriesByLetter[firstLetter]![indexPath.row]
        
        NSLog("Show some popup for \(countryName)")
    }
//4. Multiple selections
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let firstLetter = self.letters[indexPath.section]
        let countryName = self.countriesByLetter[firstLetter]![indexPath.row]
        if let currentIndex = selectedCountries.indexOf(countryName) {
            selectedCountries.removeAtIndex(currentIndex)
        } else {
            selectedCountries.append(countryName)
        }
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        //2. Cell animations
        //self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
    }
 }
