//
//  Review.swift
//  Testing
//
//  Created by Aaron Ang on 07/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import Foundation

class Review {
    var dateCreated: NSDate
    var rating: Int {
        didSet {
            if rating < 1 {
                self.rating = 1
            }else if rating > 5 {
                self.rating = 5
            }
        }
    }
    
    init(rating: Int, date: NSDate = NSDate()) {
        self.rating = rating
        self.dateCreated = NSDate()
    }
}