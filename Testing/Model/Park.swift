//
//  Park.swift
//  Testing
//
//  Created by Aaron Ang on 07/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import Foundation
import UIKit

class Park {
    var name: String
    var photo: UIImage?
    var location: String?
    var reviews: [Review] = []
    
    init(name: String) {
        self.name = name
    }
    func averageRating() -> Double {
        if reviews.count == 0 {
            return 0.0
        }
        
        var average: Double = 0.0
        for review in self.reviews {
            average += Double (review.rating)
        }
        return average / Double (reviews.count)
    }
}