//
//  ViewController.swift
//  Testing
//
//  Created by Aaron Ang on 05/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    
    @IBOutlet weak var hiButton: UIButton!
    
    @IBOutlet weak var byeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }
    

    @IBAction func sayhi(sender: AnyObject) {
        print("Say Hi now")
        helloLabel.text = "Hi again"
    }
    
    @IBAction func saybye(){
        print("Say bye now")
        helloLabel.text = "bye again"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showBlueVC(sender: AnyObject) {
        let blueVC: BlueViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BlueViewController") as! BlueViewController
        
        print(blueVC.name)
        
        blueVC.name = "Blue View Controller"
        
        self.presentViewController(blueVC,animated: true, completion:nil)
    }

    @IBAction func showGreenVC(sender: AnyObject) {
        let GreenVC: GreenViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GreenViewController") as! GreenViewController
        
        self.navigationController?.pushViewController(GreenVC, animated: true)
//        self.presentViewController(BlueVC,animated: true, completion:nil)
    }
    @IBAction func showRedVc(sender: AnyObject) {
        let RedVC: RedViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RedViewController") as! RedViewController
        
        self.navigationController?.pushViewController(RedVC, animated: true)
    }
}

