//
//  TableViewCell.swift
//  Testing
//
//  Created by Aaron Ang on 07/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
 
    var park: Park! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.nameLabel.text = self.park.name
        self.photoImageView.image = self.park.photo
    }
}

