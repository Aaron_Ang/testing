//
//  TableViewController.swift
//  Testing
//
//  Created by Aaron Ang on 06/04/2016.
//  Copyright © 2016 Aaron Ang. All rights reserved.
//

import UIKit

class TableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    //var weatherPatterns: [String] = ["Sunny", "Cloudy", "Rain", "Thurderstorm","Haze"]


    var parks: [Park] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        self.preLoadData() 
    }
    func randomParkName() -> String{
        let dataSet: [String] = ["Aya Sofya", "Collosseum", "Golden Gates Bridge", "Great Wall", "Pyramid of Giza", "Stonehenge", "Taj Mahal", "Victoria Falls"]
        let index: Int = Int(arc4random_uniform(UInt32(dataSet.count)))
        return dataSet[index]
    }
    
    func preLoadData() {
        for _ in 0 ..< 20 {
            let randomName = randomParkName()
            
            let review: Review = Review(rating: 5)
            
            let park: Park = Park(name: randomName)
            park.reviews.append(review)
            park.photo = UIImage(named: randomName)
            self.parks.append(park)

        }
        self.tableView.reloadData()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedIndexPath, animated: animated)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    //use segue(show) to show the details in another view controller
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTableDetailsViewController" {
            let destVC: TableDetailsViewController = segue.destinationViewController as! TableDetailsViewController
            
            if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
                let park: Park = self.parks[selectedIndexPath.row]
                destVC.park = park
            }
        }
    }
    
    @IBAction func addPark(sender: AnyObject) {
        let randomName = randomParkName()
        let review: Review = Review(rating: 5)
        let park: Park = Park(name: randomName)
        park.reviews.append(review)
        park.photo = UIImage(named: randomName)
        self.parks.append(park)
//        names.append(randomName)
//        images.append(UIImage(named: randomName)!)
        //let park: Park = Park.Randompark()
        //self.parks.append(park)
        self.tableView.reloadData()
    }


    
}
//extension TableViewController : UITableViewDataSource{
//    func tableView(tableView: UITableView, numberOdRowSection section:Int) -> Int {
//        return weatherPatterns.count
//    }
//    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
//        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
//        cell.textLabel.text = weatherPatterns[indexPath.row]
//        
//        return cell
//    }
//}

extension TableViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        let cell: TableViewCell = self.tableView.dequeueReusableCellWithIdentifier("TableViewCell", forIndexPath: indexPath) as! TableViewCell
        
//        cell.textLabel?.text = self.parks[indexPath.row].name
//        cell.imageView?.image = self.parks[indexPath.row].photo
        let park: Park = self.parks[indexPath.row]
        cell.park = park

        
//        cell.textLabel?.text = park.name
//        cell.imageView?.image = park.photo
//        
//        if indexPath.row % 2 == 1 {
//        cell.textLabel?.textColor = UIColor.redColor()
//        } else {
//            cell.textLabel?.textColor = UIColor.blueColor()
//        }
        return cell
    }
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return weatherPatterns.count
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
//        
//        cell.textLabel?.text = weatherPatterns[indexPath.row]
//        
//        return cell
//    }
    
}
extension TableViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //print("\(indexPath.section), \(indexPath.row)")
        //pass data forward
         let detailsVC: TableDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TableDetailsViewController") as! TableDetailsViewController
         
         let park: Park = self.parks[indexPath.row]
         detailsVC.park = park
        
         self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}